var url = require('url');
var RSVP = require('rsvp');
var redis = require("redis");

function Redis(logger, opts) {
    opts = opts || {
        url: 'redis://localhost:6379'
    };
    if (opts.url === '$DATABASE_URL') {
        opts.url = process.env.DATABASE_URL;
    }
    var self = this;
    var connectUrl = url.parse(opts.url);
    var redisOptions = {
        db: opts.db || 0
    };

    if(connectUrl.auth)
        redisOptions.auth_pass = connectUrl.auth.split(':')[1];
    else
        redisOptions.auth_pass = opts.password || null;
    
    self.client = redis.createClient(connectUrl.port, connectUrl.hostname, redisOptions);
}

var proto = Redis.prototype;

function redisKey(key, clientKey){
    return [clientKey, key].join(':');
}

proto.get = function(key, clientKey){
    var self = this;
    var promise = new RSVP.Promise(function(resolve, reject){
        self.client.get(redisKey(key,clientKey), function(err, res){
            if (!err) {
                resolve(JSON.parse(res));
            } else {
                reject(err);
            };
        });
    });
    return promise;
}

proto.set = function(key, val, clientKey){
    var self = this;
    var sval = JSON.stringify(val, null, 2);
    var promise = new RSVP.Promise(function(resolve, reject){
        self.client.set(redisKey(key,clientKey), sval, function(err, res){
            if (!err) {
                resolve(res);
            } else {
                reject(err);
            };
        });
    });
    return promise;
}

proto.del = function(key, clientKey){
    var self = this;
    var promise = new RSVP.Promise(function(resolve, reject){
        self.client.del(redisKey(key,clientKey), function(err, res){
            if (!err) {
                resolve(res);
            } else {
                reject(err);
            };
        });
    });
    return promise;
}

proto.getAllClientInfos = function() {
    var self = this;
    var promise = new RSVP.Promise(function(resolve, reject) {
        self.client.keys('*:clientInfo', function(err, keys) {
            if (err) {
                reject(err);
                return;
            }
            var promises = keys.map(function(clientInfoStr) {
                var parts = clientInfoStr.split(':');
                return self.get(parts[1], parts[0]);
            });
            RSVP.all(promises).then(function(clients) {
                resolve(clients);
            }).catch(function(reason) {
                reject(reason);
            });
        });
    });
    return promise;
};

proto.isMemoryStore = function () {
    return false;
};

module.exports = function(logger, opts) {
    return new Redis(logger, opts);
}
