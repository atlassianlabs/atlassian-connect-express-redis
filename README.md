Update October 7, 2020:

The feature provided by this project has been merged to ACE core:
https://bitbucket.org/atlassian/atlassian-connect-express/pull-requests/215/add-redis-storage-adapter

# Atlassian Connect Express Redis Adapter

This is a basic Redis adapter for Atlassian Connect Express. You can use it in place of the default JugglingDB adapter. Using this will allow you to store your Connect client information into Redis. JugglingDB provides a Redis adapter, but the problem is that your data will be stored in JugglingDB's format which is more complicated than it should be inside of Redis.

To use, simply add the following dependency in your `package.json`:

    "atlassian-connect-express-redis": "~0.1.0"

Then, in your `config.json`, specify your datastore:

    "store": {
      "adapter": "redis",
      "url": "redis://user:password@localhost:6379"
    }

Lastly, you'll need to register the adapter in your `app.js`:

    ac.store.register('redis', require('atlassian-connect-express-redis'));

That's it.

## Bugs?

[File 'em](https://bitbucket.org/atlassianlabs/atlassian-connect-express-redis/issues?status=new&status=open) or [send me a PR](https://bitbucket.org/atlassianlabs/atlassian-connect-express-redis/pull-requests).